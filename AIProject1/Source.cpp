#include <iostream>
#include <fstream>
#include <istream>
#include "Puzzle.h"
using std::cout;
using std::cin;
using std::endl;
using std::ifstream;
using std::string;
const int NUM_PUZZLES=10;
int main()
{
	ifstream fin;
	fin.open("SampleProblems.txt");
	if(fin.fail())
		exit(1);
	Puzzle p[NUM_PUZZLES];
	for(int i=0;i<NUM_PUZZLES;i++)
	{
		char* discard=new char[1];
		fin.getline(discard,1);
		string allRows[PUZZLE_SIZE];
		for(int j=0;j<PUZZLE_SIZE;j++)
		{
			string row;
			getline(fin,row);
			allRows[j]=row;
		}
		Puzzle nextPuzzle(allRows);
		p[i]=nextPuzzle;
	}
	for(int i=0;i<NUM_PUZZLES;i++)
		cout << p[i].formattedOutput() <<"\n\n";

	fin.close();
	return 0;
}