#pragma once
#include <string>
const int PUZZLE_SIZE=9;
const int SMALLER_BOX_SIZE=3;//The board is divided into smaller boards.
const char separateChar = ' ';//This is what is used to separate smaller boxes
const char blankChar = '_';//This is what is outputted when there is nothing in a square.
class Puzzle
{
public:
	Puzzle();//Default constructor creates empty board. We probably won't use this.
	Puzzle(std::string rawInput[PUZZLE_SIZE]);//Takes an array of strings, each string is one row of the puzzle. (Any character other 1-9 is treated as empty square.)
	void set(int row,int column,int value);
	std::string formattedOutput();//Outputs a string that can be printed to the string to display a sudoku board.
	//We will add more here.
private:
	char puzzle[PUZZLE_SIZE][PUZZLE_SIZE];
};

