#include "Puzzle.h"
using std::string;

Puzzle::Puzzle()
{
	for(int i = 0; i < PUZZLE_SIZE; i++)
		for(int j = 0; j < PUZZLE_SIZE; j++)
			puzzle[i][j]=blankChar;
}
Puzzle::Puzzle(string rawInput[PUZZLE_SIZE])
{
	int curPos=0;
	for(int i=0; i<PUZZLE_SIZE; i++)
	{
		for(int j=0; j<PUZZLE_SIZE;j++)
		{
			if(rawInput[i][j]!='_')
				puzzle[i][j]=rawInput[i][j];
			else
				puzzle[i][j]=blankChar;
		}
	}
}
void Puzzle::set(int row,int column,int value)
{
	puzzle[row][column]=value;
}
string Puzzle::formattedOutput()
{
	string output;
	for(int i=0; i<PUZZLE_SIZE; i++)
	{
		if(!(i%SMALLER_BOX_SIZE)&&i!=0)//Output 'x' between rows that are in different smaller boxes.
		{
			for(int i=0;i<PUZZLE_SIZE+PUZZLE_SIZE/SMALLER_BOX_SIZE;i++)
				output.push_back((separateChar));
			output.push_back('\n');
		}
		for(int j=0; j<PUZZLE_SIZE;j++)
		{
			if(!(j%SMALLER_BOX_SIZE)&&j!=0) output.push_back(separateChar);//output 'x' between column that are in different smaller boxes.

			output.push_back(puzzle[i][j]);
		}
		output.push_back('\n');
		
		
	}
	return output;
}
